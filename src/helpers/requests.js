export const injectTokenInRequest = (fetchApi, token) => {
  fetchApi.interceptors.request.use((request) => {
    if (token) {
      request.headers.common.Authorization = 'Bearer ' + token;
    }

    return request;
  });
};
