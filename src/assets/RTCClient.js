import EventEmitter from 'events';
import AgoraRTC from 'agora-rtc-sdk';

export default class RTCClient {
  constructor() {
    this.option = {
      appId: '',
      channel: '',
      uid: '',
      token: '',
    };
    this.uidRecorder = '999';
    this.clientUser = null;
    this.clientRecorder = null;
    this.localStream = null;
    this._eventBus = new EventEmitter();
  }

  joinChannel(option) {
    return new Promise((resolve, reject) => {
      this.clientUser = AgoraRTC.createClient({ mode: 'rtc', codec: 'vp8' });
      this.clientUser.init(option.appid, () => {
        console.log('init success');
        this.clientListener();
        this.clientUser.join(option.token ? option.token : null, option.channel, option.uid, (uid) => {
          console.log('join channel: ' + this.option.channel + ' success, uid: ', uid);
          this.option = {
            appid: option.appid,
            token: option.token,
            channel: option.channel,
            uid,
          };
          resolve();
        }, (err) => {
          console.error('client join failed', err);
          reject(err);
        });
      }, (err) => {
        reject(err);
        console.error(err);
      });
      console.log('[agora-vue] appId', option.appid);
    });
  }

  publishStream() {
    return new Promise((resolve, reject) => {
      // Create a local stream
      this.localStream = AgoraRTC.createStream({
        streamID: this.option.uid,
        audio: true,
        video: true,
        screen: false,
      });
      // Initialize the local stream
      this.localStream.init(() => {
        console.log('init local stream success');
        resolve(this.localStream);
        // Publish the local stream
        this.clientUser.publish(this.localStream, (err) => {
          console.log('publish failed');
          console.error(err);
        });
      }, (err) => {
        reject(err);
        console.error('init local stream failed ', err);
      });
    });
  }

  clientListener() {
    const client = this.clientUser;

    client.on('stream-added', (evt) => {
      // The stream is added to the channel but not locally subscribed
      this._eventBus.emit('stream-added', evt);
    });
    client.on('stream-subscribed', (evt) => {
      this._eventBus.emit('stream-subscribed', evt);
    });
    client.on('stream-removed', (evt) => {
      this._eventBus.emit('stream-removed', evt);
    });
    client.on('peer-online', (evt) => {
      this._eventBus.emit('peer-online', evt);
    });
    client.on('peer-leave', (evt) => {
      this._eventBus.emit('peer-leave', evt);
    });
    client.on('onTokenPrivilegeWillExpire', (evt) => {
      this._eventBus.emit('onTokenPrivilegeWillExpire', evt);
    });
    client.on('onTokenPrivilegeDidExpire', (evt) => {
      this._eventBus.emit('onTokenPrivilegeDidExpire', evt);
    });
  }

  on(eventName, callback) {
    this._eventBus.on(eventName, callback);
  }

  leaveChannelUser() {
    return new Promise((resolve, reject) => {
      // Leave the channel
      this.clientUser.unpublish(this.localStream, (err) => {
        console.log(err);
      });
      this.clientUser.leave(() => {
        // Stop playing the local stream
        if (this.localStream.isPlaying()) {
          this.localStream.stop();
        }
        // Close the local stream
        this.localStream.close();
        this.clientUser = null;
        resolve();
        console.log('client leaves channel success');
      }, (err) => {
        reject(err);
        console.log('channel leave failed');
        console.error(err);
      });
    });
  }

  joinChannelRecorder(option, optionRecorder) {
    return new Promise((resolve, reject) => {
      this.clientRecorder = AgoraRTC.createClient({ mode: 'rtc', codec: 'vp8' });
      this.clientRecorder.init(option.appid, () => {
        console.log('init success recorder');
        this.uidRecorder = optionRecorder.uid;
        this.clientRecorder.join(optionRecorder.agoraToken ? optionRecorder.agoraToken : null, option.channel, this.uidRecorder, (uid) => {
          console.log('join channel: ' + this.option.channel + ' success recorder, uid: ', uid);
          resolve();
        }, (err) => {
          console.error('client recorder join failed', err);
        });
      }, (err) => {
        reject(err);
        console.error(err);
      });
      console.log('[agora-vue] appId', option.appid);
    });
  }

  muteAudio() {
    this.localStream.muteAudio();
  }

  muteVideo() {
    this.localStream.muteVideo();
  }

  unmuteAudio() {
    this.localStream.unmuteAudio();
  }

  unmuteVideo() {
    this.localStream.unmuteVideo();
  }
}
