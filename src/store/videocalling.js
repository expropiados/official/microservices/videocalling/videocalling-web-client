import TokenService from '@/services/videocalling/token-service';
import VideocallService from '@/services/videocalling/videocalling-service';
import jwtDecode from 'jwt-decode';

export const state = () => ({
  option: {
    appid: '',
    token: '',
    uid: null,
    channel: '',
    resourceId: '',
    sid: '',
    resourceIdMix: '',
    sidMix: '',
    mode: 'individual',
    modeMix: 'mix',
    videoCallId: null,
    patientUid: '',
  },
  user: {
    userUUID: '',
    userType: 0,
    token: '',
  },
  optionRecorder: {
    agoraToken: '',
    authToken: '',
    uid: '999',
    firstTime: false,
  },
  appointmentInfo: {
    startDate: '',
    endDate: '',
    date: '',
    initHourAp: '',
    endHourAp: '',
    endHour: '',
    endMinute: '',
    endSecond: '',
    specialistName: '',
    patientName: '',
    id: '',
  },
  datesError: {
    date: '',
    initHour: '',
    endHour: '',
  },
  s3BucketInfo: {
    secretKey: '',
    accessKey: '',
    bucketName: '',
    saveDirectory: null,
    saveDirectoryMix: null,
  },
  loadingEntry: true,
});

export const actions = {
  setUser({ commit }, { user }) {
    commit('storeUser', user);
  },
  setChannel({ commit }, { channelName }) {
    commit('storeChannel', channelName);
  },
  setResourceID({ commit }, { resourceId }) {
    commit('storeResourceId', resourceId);
  },
  setSID({ commit }, { sid }) {
    commit('storeSid', sid);
  },
  setResourceIDMix({ commit }, { resourceId }) {
    commit('storeResourceIdMix', resourceId);
  },
  setSIDMix({ commit }, { sid }) {
    commit('storeSidMix', sid);
  },
  setLoadingEntry({ commit }, { loadingEntry }) {
    commit('storeLoadingEntry', loadingEntry);
  },
  async getToken({ commit }, { channelName, uid, role, videoCallDurationMs }) {
    const service = this.$getSentimentService(TokenService);
    const tokenRes = await service.getToken({ channelName, uid, role, videoCallDurationMs });
    commit('storeTokenRes', tokenRes);
    const username = '';
    const password = '';
    const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64');
    commit('storeTokenAuth', token);
  },
  async obtainVideoCallSaveDirectory({ commit }, { videoCallId, token, role }) {
    try {
      const service = this.$getVideocallingService(VideocallService);
      const status = await service.obtainVideoCallSaveDirectory({ videoCallId, recordingType: 'INDIVIDUAL', token, role });
      commit('storeVideoCallSaveDirectory', status.data);
      const statusMix = await service.obtainVideoCallSaveDirectory({ videoCallId, recordingType: 'DUAL', token, role });
      commit('storeVideoCallSaveDirectoryMix', statusMix.data);
      console.log(status);
    } catch (error) {
      console.log(error);
    }
  },
  async saveEspecialistSessionNotes({ commit }, { videoCallId, notes, token, role }) {
    try {
      const service = this.$getVideocallingService(VideocallService);
      await service.saveEspecialistSessionNotes({ videoCallId, notes, token, role });
      console.log(status);
    } catch (error) {
      console.log(error);
    }
  },
  async getEspecialistSessionNotes({ commit }, { videoCallId, token, role }) {
    try {
      const service = this.$getVideocallingService(VideocallService);
      const status = await service.getEspecialistSessionNotes({ videoCallId, token, role });
      console.log(status);
      return status.data;
    } catch (error) {
      console.log(error);
    }
  },
  async saveRecordingParameters({ commit }, { resourceId, sid, videoCallId, firstTimeAgoraTokenRecorder, folderName, token, role }) {
    try {
      const service = this.$getVideocallingService(VideocallService);
      const status = await service.saveRecordingParameters({ resourceId, sid, videoCallId, firstTimeAgoraTokenRecorder, folderName, token, role });
      commit('storeFirstTimeAgoraRecorder', false);
      console.log(status);
    } catch (error) {
      console.log(error);
    }
  },
  async makeTranscription({ commit }, { appointmentId, userId, videocallDir, videocallUuid, token, role }) {
    try {
      const service = this.$getSentimentService(VideocallService);
      const status = await service.makeTranscription({ appointmentId, userId, videocallDir, videocallUuid, token, role });
      console.log(status);
      console.log(status);
    } catch (error) {
      console.log(error);
    }
  },
  setUserInformation({ commit }) {
    const role = this.$cookiz.get('role') ? this.$cookiz.get('role') : '';
    const token = this.$cookiz.get('token') ? this.$cookiz.get('token') : '';
    const tokenDecoded = token ? jwtDecode(token) : '';
    if (tokenDecoded && token) {
      commit('storeLoginInformation', { role, tokenDecoded, token });
    }
  },
  async enterVideoCallService({ commit }, { agoraName, userType, userUUID, token }) {
    try {
      const service = this.$getVideocallingService(VideocallService);
      const videocallCredentials = await service.enterVideoCallService({ agoraName, userType, userUUID, token });
      console.log(videocallCredentials)
      commit('storeOptionsVideocall', videocallCredentials.data);
    } catch (error) {
      const errorStatus = error.request ? error.request.status : '-';
      if (errorStatus === 401) {
        this.$router.push('/error/usuario-sin-permisos');
      }
      if (errorStatus === 403) {
        const data = error.response.data;
        const initDate = data.initDate;
        const endDate = data.endDate;
        const year = initDate.substring(0, 4);
        const month = initDate.substring(5, 7);
        const day = initDate.substring(8, 10);
        const date = day + '/' + month + '/' + year;
        const initHour = initDate.substring(11, 16);
        const endHour = endDate.substring(11, 16);
        const dates = {
          date,
          initHour,
          endHour,
        };
        commit('storeErrorDates', dates);
        this.$router.push('/error/cita-fuera-de-rango');
      }
      if (errorStatus === 406) {
        return this.$nuxt.error({ error: 404, message: 'err message' });
      }
      return this.$nuxt.error({ error: 404, message: 'err message' });
    }
  },
};

export const mutations = {
  storeUser(_state, user) {
    _state.user = user;
  },
  storeFirstTimeAgoraRecorder(_state, firstTime) {
    _state.optionRecorder.firstTime = firstTime;
  },
  storeLoginInformation(_state, { role, tokenDecoded, token }) {
    if (role === 1) {
      _state.user.userType = 1;
    }
    _state.user.userUUID = tokenDecoded.us_uid;
    _state.user.token = token;
    _state.s3BucketInfo.accessKey = this.$config.accessKeyBucket;
    _state.s3BucketInfo.secretKey = this.$config.secretKeyBucket;
    _state.s3BucketInfo.bucketName = this.$config.bucketName;
  },
  storeChannel(_state, channelName) {
    _state.option.channel = channelName;
  },
  storeUID(_state, uid) {
    _state.option.uid = uid;
  },
  storeOptionsVideocall(_state, videocallCredentials) {
    _state.option.appid = videocallCredentials.appID;
    _state.option.token = videocallCredentials.token;
    _state.option.uid = videocallCredentials.userUID;
    _state.appointmentInfo.startDate = videocallCredentials.initDateFormat;
    _state.appointmentInfo.endDate = videocallCredentials.endDateFormat;
    const endHour = videocallCredentials.endDateFormat.substring(11, 13);
    const endMinute = videocallCredentials.endDateFormat.substring(14, 16);
    const endSecond = videocallCredentials.endDateFormat.substring(17, 19);
    const year = videocallCredentials.endDateFormat.substring(0, 4);
    const month = videocallCredentials.endDateFormat.substring(5, 7);
    const day = videocallCredentials.endDateFormat.substring(8, 10);
    const date = day + '/' + month + '/' + year;
    const initHourAp = videocallCredentials.initDateFormat.substring(11, 16);
    const endHourAp = videocallCredentials.endDateFormat.substring(11, 16);
    _state.appointmentInfo.date = date;
    _state.appointmentInfo.initHourAp = initHourAp;
    _state.appointmentInfo.endHourAp = endHourAp;
    _state.appointmentInfo.endHour = endHour;
    _state.appointmentInfo.endMinute = endMinute;
    _state.appointmentInfo.endSecond = endSecond;
    _state.option.videoCallId = videocallCredentials.videoCallId;
    _state.appointmentInfo.patientName = videocallCredentials.patientName;
    _state.appointmentInfo.specialistName = videocallCredentials.specialistName;
    _state.appointmentInfo.id = videocallCredentials.appointmentId;
    _state.option.patientUid = videocallCredentials.patientAgoraUID;
    if (videocallCredentials.recordingEntryData) {
      _state.option.resourceId = videocallCredentials.recordingEntryData.resourceIndividualId;
      _state.option.sid = videocallCredentials.recordingEntryData.sidIndividual;
      _state.option.resourceIdMix = videocallCredentials.recordingEntryData.resourceDualId;
      _state.option.sidMix = videocallCredentials.recordingEntryData.sidDual;
      _state.optionRecorder.agoraToken = videocallCredentials.recordingEntryData.agoraTokenRecorder;
      _state.optionRecorder.authToken = videocallCredentials.recordingEntryData.authTokenRecorder;
      _state.optionRecorder.firstTime = videocallCredentials.recordingEntryData.firstTimeAgoraTokenRecorder;
      _state.optionRecorder.uid = videocallCredentials.recordingEntryData.uidRecorder.toString();
    }
  },
  storeErrorDates(_state, errorDates) {
    _state.datesError.date = errorDates.date;
    _state.datesError.initHour = errorDates.initHour;
    _state.datesError.endHour = errorDates.endHour;
  },
  storeTokenRes(_state, tokenRes) {
    _state.optionRecorder.agoraToken = tokenRes.data.token;
  },
  storeTokenAuth(_state, tokenAuth) {
    _state.optionRecorder.authToken = tokenAuth;
  },
  storeResourceId(_state, resourceId) {
    _state.option.resourceId = resourceId;
  },
  storeSid(_state, sid) {
    _state.option.sid = sid;
  },
  storeResourceIdMix(_state, resourceId) {
    _state.option.resourceIdMix = resourceId;
  },
  storeSidMix(_state, sid) {
    _state.option.sidMix = sid;
  },
  storeVideoCallSaveDirectory(_state, videoCallSaveDirectory) {
    _state.s3BucketInfo.saveDirectory = videoCallSaveDirectory.folderName;
  },
  storeVideoCallSaveDirectoryMix(_state, videoCallSaveDirectory) {
    _state.s3BucketInfo.saveDirectoryMix = videoCallSaveDirectory.folderName;
  },
  storeLoadingEntry(_state, loadingEntry) {
    _state.loadingEntry = loadingEntry;
  },
};
