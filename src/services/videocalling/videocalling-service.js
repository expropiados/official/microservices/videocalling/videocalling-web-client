import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const VideocallServiceStamp = stampit.methods({
  enterVideoCallService({ agoraName, userType, userUUID, token }) {
    const body = {
      agoraName,
      userType,
      userUUID,
    };
    const authHeader = {
      headers: {
        Authorization: `Bearer ${token}`,
        role: userType,
      },
    };
    console.log(body);
    return this.fetchApi.post('video_call/entry', body, authHeader);
  },
  saveRecordingParameters({ resourceId, sid, videoCallId, firstTimeAgoraTokenRecorder, folderName, token, role }) {
    const body = {
      resourceId,
      sid,
      videoCallId,
      firstTimeAgoraTokenRecorder,
      folderName,
    };
    const authHeader = {
      headers: {
        Authorization: `Bearer ${token}`,
        role,
      },
    };
    console.log(body);
    return this.fetchApi.post('video_call/recording/parameters', body, authHeader);
  },
  obtainVideoCallSaveDirectory({ videoCallId, recordingType, token, role }) {
    const body = {
      videoCallId,
      recordingType,
    };
    const authHeader = {
      headers: {
        Authorization: `Bearer ${token}`,
        role,
      },
    };
    console.log(body);
    return this.fetchApi.post('video_call/recording', body, authHeader);
  },
  saveEspecialistSessionNotes({ videoCallId, notes, token, role }) {
    const body = notes;
    console.log(body);
    const authHeader = {
      headers: {
        Authorization: `Bearer ${token}`,
        role,
      },
    };
    console.log(body);
    return this.fetchApi.post(`/video_call/${videoCallId}/appointments/annotation`, body, authHeader);
  },
  getEspecialistSessionNotes({ videoCallId, token, role }) {
    const authHeader = {
      headers: {
        Authorization: `Bearer ${token}`,
        role,
      },
    };
    return this.fetchApi.get(`/video_call/${videoCallId}/appointments/annotation`, authHeader);
  },
  makeTranscription({ appointmentId, userId, videocallDir, videocallUuid, token, role }) {
    const body = {
      appointmentId,
      userId,
      videocallDir,
      videocallUuid,
    };
    const authHeader = {
      headers: {
        Authorization: `Bearer ${token}`,
        role,
      },
    };
    return this.fetchApi.post('transcribe', body, authHeader);
  },
});

const VideocallService = stampit.compose(ServiceStamp, VideocallServiceStamp);
export default VideocallService;
