import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const TokenServiceStamp = stampit.methods({
  getToken({ channelName, uid, role, videoCallDurationMs }) {
    const body = {
      channelName,
      uid,
      role,
      videoCallDurationMs
    }
    return this.fetchApi.post('videocalling', body);
  },
});

const TokenService = stampit.compose(ServiceStamp, TokenServiceStamp);
export default TokenService;