import axios from 'axios';
import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const AgoraServiceStamp = stampit.methods({
  enterVideoCallService({ agoraName, userType, userUUID }) {
    const body = {
      agoraName,
      userType,
      userUUID,
    };
    console.log(body);
    return axios.post('video_call/entry', body);
  },
});

const AgoraService = stampit.compose(ServiceStamp, AgoraServiceStamp);
export default AgoraService;
