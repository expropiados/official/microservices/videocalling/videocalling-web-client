import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const CompanyServiceStamp = stampit.methods({
  getCompany({ id }) {
    return this.fetchApi.get(`company/${id}`);
  },
});

const CompanyService = stampit.compose(ServiceStamp, CompanyServiceStamp);
export default CompanyService;
