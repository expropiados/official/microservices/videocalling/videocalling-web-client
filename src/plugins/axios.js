import { injectTokenInRequest } from '~/helpers/requests';

export default function ({ $axios, store }) {
  const { token } = store.state;

  $axios.setBaseURL(process.env.apiBaseUrl);
  injectTokenInRequest($axios, token);
}
