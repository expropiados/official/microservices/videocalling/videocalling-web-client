# Videocalling Web Client
Frontend para el servicio de videollamadas.

## Sonarcloud
```text
https://sonarcloud.io/dashboard?id=expropiados_videocalling-web-client
```

## Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
