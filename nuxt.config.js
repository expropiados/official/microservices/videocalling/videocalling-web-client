import colors from 'vuetify/es5/util/colors';

export default {
  srcDir: 'src/',

  publicRuntimeConfig: {
    apiBaseUrl: process.env.BASE_URL || 'http://127.0.0.1:3000',
    attentionUrl: process.env.SERVICE_ATTENTION_API_URL || 'http://127.0.0.1:3000',
    sentimentUrl: process.env.SERVICE_SENTIMENT_API_URL || 'http://127.0.0.1:3000',
    backofficeUrl: process.env.SERVICE_BACKOFFICE_API_URL || 'http://127.0.0.1:3000',
    videocallingUrl: process.env.SERVICE_VIDEOCALLING_API_URL || 'http://127.0.0.1:3000',
    securityUrl: process.env.SERVICE_SECURITY_API_URL || 'http://127.0.0.1:3000',
    secretKeyBucket: process.env.SECRET_KEY_BUCKET,
    accessKeyBucket: process.env.ACCESS_KEY_BUCKET,
    bucketName: process.env.BUCKET_NAME,
    ctAttentionUrl: process.env.ATTENTION_URL || 'http://127.0.0.1:3000',
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s',
    title: 'Videollamadas',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/axios.js',
    '~/plugins/services.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // With options
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      themes: {
        light: {
          primary: '#1ECD96',
          secondary: colors.grey.darken3,
          accent: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
};
